# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/dmacvicar/libvirt" {
  version     = "0.6.11"
  constraints = "0.6.11"
  hashes = [
    "h1:6QzHY/7aNdaaDxJZKygotWnM5uHoS2gs/03CzUCJX60=",
    "zh:15300a1c3c294eccade4c8a678412d81602ab041dc0a5aab72fee5425d778e89",
    "zh:1605806de0d3b86b7e94b5d04a7ad9b6ac695781f9672ab6002c23caef43b98e",
    "zh:21efc5937d89f9ec96bc626d2ce3621c0919b3da97ab63b4e520c37d3f5c7357",
    "zh:2c143a6909917fd11191447de4c496f084c7da5200beb9f512791a80a1f33e7c",
    "zh:3ca369718cc49feefc3a6ffa795a9055e60de33989a9f1c72b6db16048a181fa",
    "zh:71db1d1cf2c06984bba408ad5dc9b4e25285684ee5c530a61583b202cff21b96",
    "zh:a67adfc988311d34adcc119500c2ef048a45d632b00bb5a15ea6d3ffdc1c3d1c",
    "zh:a83448cbcc194e3b52af9b89b9273a116082d83f2c966035bf8a8c5d5606ca9c",
    "zh:a9c5a818dd2606460d4d6f33af7cb387f3e984d631fc233aaec0dda4e0756c2b",
    "zh:af3263e66cf9138361d6d7408533edd6de8498e67c88cf0084421ae31fe89054",
    "zh:b9596cb26c1e391172472de4ada9b3b0a08e4777e41327db8e021454cc6aae20",
    "zh:e28124b9ee0b8c18b6f776eb6523d8935f3072c47cd803ea2f1a06206effaa48",
    "zh:edd00638d8c088b8a38e7ab8b9e8ab3bd710f7357d0f6b4a38e0028bd49d8460",
  ]
}
