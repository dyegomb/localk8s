terraform {
  required_providers {
    libvirt = {
      # https://github.com/dmacvicar/terraform-provider-libvirt
      # https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs
      source = "dmacvicar/libvirt"
      version = "~> 0.6"
    }
  }
}

# Configure the Libvirt provider
provider "libvirt" {
  uri = "qemu:///system"
}

# Pool for terraform
resource "libvirt_pool" "terraform" {
  name = "terraform"
  type = "dir"
  path = "/home/dyego/VMs/terraform"
}

# Disk base
resource "libvirt_volume" "disk_base" {
  name   = "disk_base"
  #source = "/home/dyego/VMs/debian11.qcow2"
  source = "/home/dyego/VMs/centos-stream8_base.qcow2"
  pool = libvirt_pool.terraform.name
}

# master disk
resource "libvirt_volume" "master" {
  name           = "master.qcow2"
  base_volume_id = libvirt_volume.disk_base.id
  pool = libvirt_pool.terraform.name
}
variable "node_count" {
  type = number
  description = "How many nodes? (10 or less)"
  default = 2
}

# workers disks
resource "libvirt_volume" "node" {
  name           = "k8sn${count.index}.qcow2"
  base_volume_id = libvirt_volume.disk_base.id
  count          = var.node_count
  pool = libvirt_pool.terraform.name
}

# master domain
resource "libvirt_domain" "k8s-m" {
  name = "k8sm"
  memory = 2048 
  vcpu = 2
  arch = "x86_64"
  emulator = "/usr/bin/qemu-system-x86_64"
  cpu {
    mode = "host-passthrough"
  }
  disk {
    volume_id = libvirt_volume.master.id
  }
  network_interface {
    hostname = "k8sm"
    addresses = ["192.168.200.80/24"]
    wait_for_lease = false
    mac = "52:54:00:a6:7f:01"
    bridge = "virbr0"
  }
}

# nodes domains
resource "libvirt_domain" "node" {
  name = "k8sn${count.index}"
  memory = 1024
  vcpu = 2
  arch = "x86_64"
  emulator = "/usr/bin/qemu-system-x86_64"
  count = var.node_count
  cpu {
    mode = "host-passthrough"
  }
  disk {
    volume_id = libvirt_volume.node[count.index].id
  }
  network_interface {
    addresses = ["192.168.200.9${count.index}/24"]
    wait_for_lease = false
    mac = "52:54:00:a6:7f:1${count.index}"
    bridge = "virbr0"
  }
}