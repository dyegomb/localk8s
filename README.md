# [ Work In Progress ]
# Kubernetes The IaC Way
Inspired by [Kelsey Hightower's "Kubernetes The Hard Way"](https://github.com/kelseyhightower/kubernetes-the-hard-way), this git project aims to fully install Kubernetes on local VMs managed by libvirt.


## TO DO
- [x] install kubernetes services and utilities
- [ ] use kata/firecracker
- [ ] configure kubernetes cluster

### Referencies
- https://github.com/kelseyhightower/kubernetes-the-hard-way
- https://www.padok.fr/en/blog/deploy-kubernetes-firecracker
- https://github.com/kata-containers/kata-containers/tree/main/docs/install
- https://github.com/duzhanyuan/katadocumentation/blob/master/how-to/how-to-use-k8s-with-cri-containerd-and-kata.md